"struct electronic_states()
  just creates the fermionic states empty, up, down, full
"

struct ElectronicStates
    em
    up
    dn
    fl
    function ElectronicStates()
        em = [1.0 0 0 0] #empty state
        up = [0 1.0 0 0] #spin up
        dn = [0 0 1.0 0] #spin down
        fl = [0 0 0 1.0] #full
        new(em, up, dn, fl)
    end
end


"function ElectronicOperators(n_sites)
  create single-sites electronic operators acting on chain with length n_sites
"
struct ElectronicOperators
    Cdag_up
    C_up
    Cdag_dn
    C_dn
    N_up
    N_dn
    N_tot
    Fup
    Fdn
    Id
    function ElectronicOperators(n_sites)
        st = ElectronicStates()

        #define states and operators which will be used afterwards
        em = st.em
        up = st.up
        dn = st.dn
        fl = st.fl

        ii = I(4) #identity matrix
        f = [1 0 0 0; 0 -1 0 0; 0 0 -1 0; 0 0 0 1] #fermi factor
        fup = [1 0 0 0; 0 -1 0 0; 0 0 1 0; 0 0 0 -1]
        fdn = [1 0 0 0; 0 1 0 0; 0 0 -1 0; 0 0 0 -1]
        #define operators acting on the chain. We use the "non-i" version 
        #for single operators, and "with-i" for operators on the chain inside this
        #function. For simplicity, the field of the struct are in "non-i" version
        #even if they act on the full chain
        Cdag_up = transpose(up)*em + transpose(fl)*dn
        C_up = transpose(em)*up + transpose(dn)*fl
        Cdag_dn = transpose(dn)*em + transpose(fl)*up
        C_dn = transpose(em)*dn + transpose(up)*fl

        N_up = Cdag_up*C_up
        N_dn = Cdag_dn*C_dn
        N_tot = N_up + N_dn

        Cdag_upi = zeros(Float64, (n_sites,4^n_sites,4^n_sites))
        C_upi = zeros(Float64, (n_sites,4^n_sites,4^n_sites))
        Cdag_dni = zeros(Float64, (n_sites,4^n_sites,4^n_sites))
        C_dni = zeros(Float64, (n_sites,4^n_sites,4^n_sites))

        N_upi = zeros(Float64, (n_sites,4^n_sites,4^n_sites))
        N_dni = zeros(Float64, (n_sites,4^n_sites,4^n_sites))
        N_toti = zeros(Float64, (n_sites,4^n_sites,4^n_sites))

        Fupi = zeros(Float64, (n_sites,4^n_sites,4^n_sites))
        Fdni = zeros(Float64, (n_sites,4^n_sites,4^n_sites))
        Idi = zeros(Float64, (n_sites,4^n_sites,4^n_sites))
        for i in 1:n_sites
                Cdag_upi[i,:,:] = kron((f for j in 1:i-1)...,Cdag_up,(ii for j in i:n_sites-1)...)
                C_upi[i,:,:] = kron((f for j in 1:i-1)...,C_up,(ii for j in i:n_sites-1)...)
                Cdag_dni[i,:,:] = kron((f for j in 1:i-1)...,f*Cdag_dn,(ii for j in i:n_sites-1)...)
                C_dni[i,:,:] = kron((f for j in 1:i-1)...,f*C_dn,(ii for j in i:n_sites-1)...)

                N_upi[i,:,:] = kron((ii for j in 1:i-1)...,N_up,(ii for j in i:n_sites-1)...)
                N_dni[i,:,:] = kron((ii for j in 1:i-1)...,N_dn,(ii for j in i:n_sites-1)...)
                N_toti[i,:,:] = kron((ii for j in 1:i-1)...,N_tot,(ii for j in i:n_sites-1)...)
                
                Fupi[i,:,:] = kron((fup for j in 1:i-1)...,fup,(ii for j in i:n_sites-1)...)
                Fdni[i,:,:] = kron((fdn for j in 1:i-1)...,fdn,(ii for j in i:n_sites-1)...)
                Idi[i,:,:] = kron((ii for j in 1:i-1)...,ii,(ii for j in i:n_sites-1)...) #ToDo: write it simpler!, and put it as single!
                
        end
        new(Cdag_upi, C_upi, Cdag_dni, C_dni, N_upi, N_dni, N_toti, Fupi, Fdni, Idi)
    end
        
end

struct ElectronicOperatorsFunc
    Cdag_up
    C_up
    Cdag_dn
    C_dn
    N_up
    N_dn
    N_tot
    Fup
    Fdn
    Id
    function ElectronicOperatorsFunc(n_sites)
        op = ElectronicOperators(n_sites) #operators
        new(x::Int -> op.Cdag_up[x,:,:], x::Int -> op.C_up[x,:,:], x::Int -> op.Cdag_dn[x,:,:],
        x::Int -> op.C_dn[x,:,:], x::Int -> op.N_up[x,:,:], x::Int -> op.N_dn[x,:,:],
        x::Int -> op.N_tot[x,:,:], x::Int -> op.Fup[x,:,:],  x::Int -> op.Fdn[x,:,:], x::Int -> op.Id[x,:,:])
    end
end


""" function electronic_hamiltonian_builder(e, t, U)
   e: on-site energy terms
   t: matrix with hopping coefficients
   U: matrix with Coulomb repulsion term
   U_prime: intraleyer coupling. ToDo: write it more elegantly
"""
function electronic_hamiltonian_builder(e,t,U) 
    n_sites = length(e)

    op = ElectronicOperatorsFunc(n_sites)

    H = zeros(typeof(e[1]*U[1][1]*t[1][1]),(4^n_sites, 4^n_sites))

    for i in 1:n_sites
        H .+= e[i]*op.N_tot(i)
        for j in 1:n_sites
            H .+= t[i,j][1] * op.Cdag_up(i) * op.C_up(j)
            H .+= t[i,j][1] * op.Cdag_dn(i) * op.C_dn(j)
            H .+= U[i,j][1] * op.N_up(i) * op.N_dn(j)
        end
    end
    return H
end


function electronic_hamiltonian_builder(e,t,U, U_prime) 
    n_sites = length(e)

    op = ElectronicOperatorsFunc(n_sites)

    H = zeros(typeof(e[1]*U[1][1]*t[1][1]),(4^n_sites, 4^n_sites))

    for i in 1:n_sites
        H .+= e[i]*op.N_tot(i)
        for j in 1:n_sites
            H .+= t[i,j][1] * op.Cdag_up(i) * op.C_up(j)
            H .+= t[i,j][1] * op.Cdag_dn(i) * op.C_dn(j)
            H .+= U[i,j][1] * op.N_up(i) * op.N_dn(j)
            H .+= U_prime[i,j][1] * op.N_up(i) * op.N_up(j)
            H .+= U_prime[i,j][1] * op.N_dn(i) * op.N_dn(j)
        end
    end
    return H
end